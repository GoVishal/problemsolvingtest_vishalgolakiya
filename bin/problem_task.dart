import 'package:problem_task/distinct_value.dart';
import 'package:problem_task/frog_escape_task.dart';
import 'package:problem_task/largest_number.dart';
import 'package:problem_task/sort_list.dart';
import 'package:problem_task/string_task.dart';

void main(List<String> arguments) {
  print('Sort task :: ${sortList([5, 2, 7, 3, 8, 50, 500, 199])}');
  print('Largest number task :: ${getLargestNumber([1, 4, 17, 7, 25, 3, 100], 3)}');
  print('Distinct value :: ${countDistinct([4, 7, 2, 3, 2, 7, 4])}');
  print('Frog escape time :: ${countFrogEscapeDays(7)}');
  print('Big string :: ${smallString('this is a big string',
      ["this","you","is","a","bigger","string","noted"])}');
}
