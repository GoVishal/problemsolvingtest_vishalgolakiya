/// Write a function that, given a list, returns
/// the number of distinct values in a given list.

int countDistinct(List<int> numberList) {
  if(numberList.isEmpty) {
    return 0;
  }
  final tmp = numberList.toSet();
  return tmp.length;
}