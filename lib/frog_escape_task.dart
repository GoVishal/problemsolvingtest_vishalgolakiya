/// A frog is at the bottom of a X metre well.
/// Each day he summons enough energy for a 3 metre leap up the well.
/// Exhausted, he then hangs there for the rest of the day. At night,
/// while he is asleep, he slips 2 metres backwards.
///
/// Write a function that takes depth of metre i.e.
/// X and calculates days it will take him to escape from the well provided
/// that X will always be a positive number.
///

int countFrogEscapeDays(int waterHeight) {
  if (waterHeight < 0) {
    throw Exception('Please provide a positive water height.');
  }
  if (waterHeight < 3) return 1;
  return waterHeight - 2;
}
