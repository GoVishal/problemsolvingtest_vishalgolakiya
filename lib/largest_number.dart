/// Write a program to find the k largest elements in a given list.
/// Elements in the list can be in any order.

List<int> getLargestNumber(List<int> numberList, int count) {
  if(numberList.length < count){
    throw Exception('Number list length cannot be smaller then count');
  }

  numberList.sort();
  return numberList
      .sublist(numberList.length - count, numberList.length)
      .reversed
      .toList();
}
