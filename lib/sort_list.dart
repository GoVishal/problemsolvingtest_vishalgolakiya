/// 1. Sort a list using a single loop and without sort function.

List<int> sortList(List<int> unsortedList) {
  if(unsortedList.isEmpty){
    throw Exception('Unsorted list cannot be smaller then 2 number');
  }

  if(unsortedList.length == 1) {
    return unsortedList;
  }

  for (int k = 0; k < unsortedList.length - 1; k++)
  {
    if (unsortedList[k] > unsortedList[k + 1])
    {
      int temp = unsortedList[k];
      unsortedList[k] = unsortedList[k + 1];
      unsortedList[k + 1] = temp;

      k = -1;
    }
  }
  return unsortedList;
}