/// Write a function that takes in a "big" string and an list of "small" strings,
/// all of which are smaller in length than the big string.
/// The function should return a list of booleans,
/// where each boolean represents whether or not the small string
/// at that index in the list of small strings is contained in the big string.
///
/// Example
/// Input:"this is a big string", ["this","you","is","a","bigger","string","noted"]
/// Output: [true, false, true, true, false, true, false]
///

List<bool> smallString(String sentence, List<String> parts){
  if(sentence.isEmpty || parts.isEmpty){
    throw Exception('Provide non empty sentence or parts list');
  }
  final List<bool> returnable = [];

  for(String element in parts){
    returnable.add(sentence.contains(element));
  }

  return returnable;
}

