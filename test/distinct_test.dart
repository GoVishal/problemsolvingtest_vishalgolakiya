import 'package:problem_task/distinct_value.dart';
import 'package:test/expect.dart';
import 'package:test/scaffolding.dart';

main() {
  test('Distinct value test', (){
    expect(countDistinct([4, 7, 2, 3, 2, 7, 4]), 4);
  });

  test('Distinct value empty test', (){
    expect(countDistinct([]), 0);
  });
}