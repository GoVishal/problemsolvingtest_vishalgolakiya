import 'package:problem_task/frog_escape_task.dart';
import 'package:test/expect.dart';
import 'package:test/scaffolding.dart';

main() {
  test('Frog escape test for 1 meter', (){
    expect(countFrogEscapeDays(1), 1);
  });

  test('Frog escape test 3 meter ', (){
    expect(countFrogEscapeDays(3), 1);
  });

  test('Frog escape test 7 meter ', (){
    expect(countFrogEscapeDays(7), 5);
  });


}
