import 'dart:math';

import 'package:problem_task/largest_number.dart';
import 'package:test/expect.dart';
import 'package:test/scaffolding.dart';

main() {
  test('largest number test', () {
    expect(getLargestNumber([1, 4, 17, 7, 25, 3, 100], 3), [100, 25, 17]);
  });

  test('largest number test', () {
    expect(getLargestNumber([1, 4, 17], 3), [17, 4, 1]);
  });

  test('largest number test', () {
    expect(getLargestNumber([], 0), []);
  });
}
