import 'package:problem_task/sort_list.dart';
import 'package:test/test.dart';

main() {
  test('shorted list test', () {
    expect(sortList([5, 4, 1, 3, 6, 2]), [1, 2, 3, 4, 5, 6]);
  });
}