import 'package:problem_task/string_task.dart';
import 'package:test/expect.dart';
import 'package:test/scaffolding.dart';

main() {
  test('Big to small string test', () {
    expect(
        smallString('this is a big string',
            ["this", "you", "is", "a", "bigger", "string", "noted"]),
        [true, false, true, true, false, true, false]);
  });
}
